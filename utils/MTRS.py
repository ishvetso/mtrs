#define PORTNUMBER 59999
#define MAXCLIENTS    10
#define MAXCONNECTS        3
#define BUFFSIZE         4096
#define CONVERSATIONLEN    16
#define HOST            "raspidevel.cern.ch"

import sys
import socket
import struct
from datetime import datetime

HOST = 'raspidevel'
PORT = 59999

LTCS = 'BBBBI'
HDRS = '@III'
SOCK = HDRS
# We build a format string for 6 channels per chip, 4 chips per board, 5 boards 
for index in range(0, 6*4*1):
    SOCK = SOCK + LTCS
sensorType = {10:"Pt10",11:"Pt50",12:"Pt100",13:"Pt200",14:"Pt500",15:"Pt1000"}

myData = struct.Struct(SOCK)
print(myData.size, struct.calcsize(SOCK))
howMuch = struct.calcsize(SOCK)

webpage = open("/var/www/html/mtrs/table-MTRS.html","w+")
webpage.truncate(0)
webpage.write("<table style=\"width:100%\" border=\"1\" id=\"summaryTable\">\n")
webpage.write("<tr align=left>\n")
webpage.write("<th>Time</th>\n")
webpage.write("<th>Channel</th>\n")
webpage.write("<th>Board ID</th>\n")
webpage.write("<th>Chip ID</th>\n")
webpage.write("<th>Channel ID</th>\n")
webpage.write("<th>Type</th>\n")
webpage.write("<th>Flag</th>\n")
webpage.write("<th>Value</th>\n")
webpage.write("</tr>\n")


with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))
    buffer = bytearray(howMuch)
    welcome = bytearray(32)
    s.sendall(b'GIME')
    gotMesg = s.recv_into(welcome, 32)
    print("Welcome message is", welcome)

    s.sendall(b'GIME')
    gotMany = s.recv_into(buffer, howMuch, socket.MSG_WAITALL)
    print('received', buffer)
    print('Expecting', howMuch)
    print('Received', gotMany)
    PayLoad = myData.unpack(buffer)
    print(PayLoad)
    print("Processor ID is", PayLoad[0])
    print("Channel Count is", PayLoad[1])
    now = datetime.fromtimestamp(PayLoad[2])
    print("Timestamp is", now)
    

    for rtd in range(24):
        step = rtd * 5
        channel = (str) (rtd)
        boardID = (str)(PayLoad[step + 3])
        chipID = (str) (PayLoad[step + 4]//16)
        channelID = (str) (PayLoad[step + 4]%16)
        TYPE = (str) (sensorType.get(PayLoad[step + 5], "Unknown"))
        Flag = (str) (PayLoad[step + 6])
        Value = (str)(PayLoad[step + 7]/1024)
        
        webpage.write('<tr>\n')
        webpage.write("<td>" + (str)(now) + '</td>\n')
        webpage.write("<td>" + channel + '</td>\n')
        webpage.write("<td>" +  boardID  + '</td>\n')
        webpage.write("<td>" + chipID  + '</td>\n')
        webpage.write("<td>" + channelID + '</td>\n')
        webpage.write("<td>" + TYPE + '</td>\n')
        webpage.write("<td>" + Flag + '</td>\n')
        webpage.write("<td>" + Value + '</td>\n')
        webpage.write('</tr>')
    webpage.write('</table>')
    webpage.close()
