import cx_Oracle
import config
from datetime import datetime


class MTRSDBUtils:
  def __init__(self, connection_, config_):
    self.connection = connection_
    self.config = config_
    try:
      self.connection = cx_Oracle.connect(
      self.config.username,
      self.config.password,
      self.config.dsn,
      encoding=self.config.encoding)
      print("Connection to the database, version ", self.connection.version)
    except cx_Oracle.Error as error:
      print(error)
  def insert_values(self, channelid, flag, time, value):
    try:
      with self.connection.cursor() as cursor:
        sql = "INSERT INTO MTRS_DATA(CHANNELID, FLAG, UPDATETIME, VALUE) VALUES (:1, :2, :3, :4)"
        cursor.execute(sql, (channelid, flag, time, value))
        self.connection.commit()
    except cx_Oracle.Error as error:
      print('Error occurred:')
      print(error)
  def get_channelid(self, boardnumber, chipnumber, channelnumber, systemname):
    with self.connection.cursor() as cursor:
      sql = "SELECT CHANNELID FROM MTRS_CHANNELS WHERE BOARDNUMBER=:1 AND CHIPNUMBER=:2 AND CHANNELNUMBER=:3 AND SYSTEMNAME=:4"
      cursor.execute(sql, (boardnumber, chipnumber, channelnumber, systemname))
      rows = cursor.fetchall()
      try:
        if len(rows)!=1:
          raise NotImplementedError
        row = rows[0]
        channelid = row[0]
        return channelid
      except NotImplementedError:
       print("The channelid must be unique")
  def register_channel(self, alias, boardnumber, chipnumber, channelnumber, sensortype, systemname):
    with self.connection.cursor() as cursor:
      cursor.callproc("register_channel", (alias, boardnumber, chipnumber, channelnumber, sensortype, systemname))
      self.connection.commit()
  def channel_exists_in_database(self, boardnumber, chipnumber, channelnumber, systemname):
    with self.connection.cursor() as cursor:
      sql = "SELECT CHANNELID FROM MTRS_CHANNELS WHERE BOARDNUMBER=:1 AND CHIPNUMBER=:2 AND CHANNELNUMBER=:3 AND SYSTEMNAME=:4"
      cursor.execute(sql, (boardnumber, chipnumber, channelnumber, systemname))
      rows = cursor.fetchall()
      if(len(rows)==1):
        return True
      elif (len(rows)==0):
        return False
      else:
        raise  NotImplementedError
  def __del__(self):
    if self.connection:
      self.connection.close()

connection = None
myMTRSUtils = MTRSDBUtils(connection, config)
boards = [34]
chips = [0,1,2,3]
channels = [0,1,2,3,4,5]
count = 0
for board in boards:
  for chip in chips:
    for channel in channels:
      exists = myMTRSUtils.channel_exists_in_database(board, chip, channel, "cms_trk_dcs_1")
      if not exists:
        count += 1
        myMTRSUtils.register_channel('channel ' + str(count), board, chip, channel,"Pt100", "cms_trk_dcs_1")
        print('channel ' + str(count), board, chip, channel,"Pt100", "cms_trk_dcs_1")

del myMTRSUtils

